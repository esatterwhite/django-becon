INSTALLATION
===========

import beckon into the root url conf, and call beckon.autodiscover

AVAILABLE SETTINGS
==================
SIGNALS_MODULE: The name of the modules which will contain signal handlers, not definitions ( "signals" )
SIGNAL_PATHS: a list or tuple of additional import paths to modules that are not named the same as SIGNALS_MODULE which should also be loaded ( [] )