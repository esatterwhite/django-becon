from django.conf import settings
from django.utils.importlib import import_module
from django.utils.module_loading import module_has_submodule

SIGNALS_MODULE = getattr( settings, "SIGNALS_MODULE", "listeners" )
SIGNAL_PATHS   = getattr( settings, "SIGNAL_PATHS", [] )


for path in ( list( SIGNAL_PATHS ) ):
    try:
        import_module( path )
        print "loaded %s" % path
    except ImportError:
        print "Failed to load %s" % path

def autodiscover( ):

    for app in ( list( settings.INSTALLED_APPS )  ):
        mod = import_module( app )
        if module_has_submodule(mod, SIGNALS_MODULE ):
            sig_mod = "%s.%s" % ( app, SIGNALS_MODULE) 
            print "loading %s" % sig_mod
            import_module( sig_mod )


