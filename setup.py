from setuptools import setup


setup(
    name="django-beckon",
    version="0.0.1",
    description="Looks for any modules names signals and attempts to load it so django picks it up",
    author="Eric Satterwhite",
    author_email="eric.satterwhite@corvisa.com",
    packages=[
        "beckon"
    ],

    classifiers=[
        "Framework::Django",
        "Programming Language::Python2.6"
    ]
)